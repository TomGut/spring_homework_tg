package pl.codementors.spring_homework_tg.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.codementors.spring_homework_tg.models.User;
import pl.codementors.spring_homework_tg.models.UserViews;
import pl.codementors.spring_homework_tg.repositories.Repo;
import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@CrossOrigin // for angular
public class MyRestController {

    @Autowired Repo repo;

    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public List<User> getAll(){
        List<User> users = repo.findAll();
        return users;
    }

    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getSingleUser(@PathVariable("id") String id){
        return repo.findOne(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public User add(@RequestBody User user){
        User u = new User();
        u.setId(user.getId());
        u.setUsername((user.getUsername()));
        u.setPassword(user.getPassword());
        u.setRole(user.getRole());
        repo.save(u);
        return u;
    }

    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/user/delete-{id}", method = RequestMethod.DELETE)
    public User delete(@PathVariable("id") String id){
        User u = repo.findOne(id);
        repo.delete(u);
        return u;
    }

    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping(value = "/user/update-{id}", method = RequestMethod.PUT)
    public User update(@PathVariable("id") String id, @RequestBody User user){
        User u = repo.findOne(id);
        u.setId(user.getId());
        u.setUsername((user.getUsername()));
        u.setPassword(user.getPassword());
        u.setRole(user.getRole());
        repo.save(u);
        return u;
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/user/role-{id}", method = RequestMethod.GET)
    @JsonView(UserViews.Role.class)
    public User getRole(@PathVariable("id") String id){
        User u = repo.findOne(id);
        return u;
    }
}