package pl.codementors.spring_homework_tg.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.codementors.spring_homework_tg.models.User;
import pl.codementors.spring_homework_tg.repositories.Repo;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServ implements UserDetailsService {

    private Repo repo;

    public UserDetailsServ(Repo repo) {
        this.repo = repo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Optional<User> user = repo.findByUsername(username);
        return user
                .map(this::mapToUser).orElseThrow(() -> new UsernameNotFoundException("Brak użytkownika o imieniu" + username + " w bazie."));
    }

    private org.springframework.security.core.userdetails.User mapToUser(final User u) {
        return new org.springframework.security.core.userdetails.User(u.getUsername(), u.getPassword(), Collections.singleton(new SimpleGrantedAuthority(u.getRole())));
    }
}