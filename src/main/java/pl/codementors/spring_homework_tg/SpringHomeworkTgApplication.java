package pl.codementors.spring_homework_tg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class SpringHomeworkTgApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringHomeworkTgApplication.class, args);
	}

}
