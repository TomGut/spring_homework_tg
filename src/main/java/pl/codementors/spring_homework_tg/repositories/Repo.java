package pl.codementors.spring_homework_tg.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.codementors.spring_homework_tg.models.User;

import java.util.List;
import java.util.Optional;

public interface Repo extends CrudRepository<User, String> {
    List<User> findAll();
    Optional<User> findByUsername(String username);
}